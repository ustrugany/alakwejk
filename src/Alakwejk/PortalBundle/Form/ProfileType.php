<?php

namespace Alakwejk\PortalBundle\Form;

use Alakwejk\PortalBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email')
            ->add('username', 'text', [
                'label' => 'Nazwa użytkownika',
            ])
            ->add('plainPassword', 'text', [
                'label' => 'Hasło',
                'required' => false,
            ])
            ->add('document', (new DocumentUploadType()), [
                'required' => false,
                'label' => 'Dokument regulaminu (.pdf, .doc, maks. 4 MB)',
            ])
            ->add('logo', (new ImageUploadType()), [
                'required' => false,
                'label' => 'Plik graficzny logo (.jpg, .png, maks. 4 MB)',
            ])
            ->add('background', 'text', [
                'label' => 'Kolor tła',
                'required' => false,
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Alakwejk\PortalBundle\Entity\User',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'profile_type';
    }
}
