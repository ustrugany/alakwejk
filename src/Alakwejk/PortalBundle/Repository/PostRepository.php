<?php

namespace Alakwejk\PortalBundle\Repository;

use Doctrine\ORM\EntityRepository;

use Alakwejk\Portal\Post\PostRepositoryInterface;
use Alakwejk\Portal\User\UserInterface;
use Alakwejk\PortalBundle\Entity\Post;

class PostRepository extends EntityRepository implements PostRepositoryInterface
{
    /**
     * @return Post[]
     */
    public function findPublishedOrderedByCreatedAt()
    {
        return $this->createQueryBuilder('e')
            ->where('e.published = true')
            ->orderBy('e.createdAt', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param UserInterface $user
     *
     * @return Post[]
     */
    public function findUsersPublishedOrderedByCreatedAt(UserInterface $user)
    {
        return $this->createQueryBuilder('e')
            ->where('e.published = true')
            ->andWhere('e.user = :user')
            ->setParameter('user', $user)
            ->orderBy('e.createdAt', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
