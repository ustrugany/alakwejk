<?php

namespace Alakwejk\PortalBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Alakwejk\Portal\Upload\UploadRepositoryInterface;

class UploadRepository extends EntityRepository implements UploadRepositoryInterface
{
}
