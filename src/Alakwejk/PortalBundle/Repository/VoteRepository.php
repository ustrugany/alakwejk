<?php

namespace Alakwejk\PortalBundle\Repository;

use Alakwejk\Portal\Post\PostInterface;
use Alakwejk\Portal\Vote\VoteRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class VoteRepository extends EntityRepository implements VoteRepositoryInterface
{
    /**
     * @param PostInterface $post
     */
    public function findPostRating(PostInterface $post)
    {
        throw new \RuntimeException('Not implemented yet');
    }
}
