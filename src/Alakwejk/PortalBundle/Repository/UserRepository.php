<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Repository;

use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Entity\User;

class UserRepository extends EntityRepository
{
    /**
     * @return User[]
     */
    public function findAllUsers()
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%ROLE_USER%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $id
     *
     * @return User
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findById($id)
    {
        return $this->createQueryBuilder('u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param int $id
     *
     * @return User
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findUserById($id)
    {
        return $this->createQueryBuilder('u')
            ->where('u.id = :id')
            ->andWhere('u.roles LIKE :roles')
            ->setParameters([
                'id' => $id,
                'roles' => '%ROLE_USER%',
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}
