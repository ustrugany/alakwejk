<?php

/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace Alakwejk\PortalBundle\Controller;

use Alakwejk\PortalBundle\Post\Exception\PostSaverException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Alakwejk\PortalBundle\Entity\Post;
use Symfony\Component\HttpFoundation\Request;

class PostController extends BaseController
{
    /**
     * @Route("/", name="alakwejk_post_list")
     *
     * @Method({"GET"})
     * @Template(":Post:index.html.twig")
     */
    public function indexAction()
    {
        $posts = $this->getPostRepository()->findPublishedOrderedByCreatedAt();

        return [
            'posts' => $posts,
        ];
    }

    /**
     * @Route("/{id}", name="alakwejk_post_show", requirements={"id": "\d+"})
     *
     * @Method({"GET"})
     * @ParamConverter("post", class="AlakwejkPortalBundle:Post", options={"mapping": {"id": "id"}})
     * @Template(":Post:show.html.twig")
     */
    public function showAction(Post $post)
    {
        return [
            'post' => $post,
        ];
    }

    /**
     * @Route("/create", name="alakwejk_post_create")
     * @Template(":Post:create.html.twig")
     */
    public function createAction(Request $request)
    {
        $post = $this->createPost();
        $form = $this->createForm('post_type', $post);

        if ($form->handleRequest($request)->isValid()) {
            try {
                $this->getPostSaver()->save($post);
                $this->addSuccessMessage('Poprawnie dodano nowy post');
            } catch (PostSaverException $exception) {
                $this->logException($exception);
                $this->addErrorMessage('Wystąpiły problemy przy dodawaniu nowego postu.');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @return \Alakwejk\PortalBundle\Repository\PostRepository
     */
    private function getPostRepository()
    {
        return $this->container->get('alakwejk.post.repository');
    }

    /**
     * @return \Alakwejk\PortalBundle\Post\PostSaver
     */
    private function getPostSaver()
    {
        return $this->get('alakwejk.post.saver');
    }

    /**
     * @return \Alakwejk\Portal\Post\PostInterface
     */
    private function createPost()
    {
        return $this->get('alakwejk.post.builder')->create();
    }
}
