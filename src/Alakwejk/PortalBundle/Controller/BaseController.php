<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;

class BaseController extends Controller
{
    const MESSAGE_DANGER_TYPE = 'danger';
    const MESSAGE_INFORMATION_TYPE = 'info';
    const MESSAGE_WARNING_TYPE = 'warning';
    const MESSAGE_SUCCESS_TYPE = 'success';

    /**
     * @param \Exception $exception
     */
    protected function logException(\Exception $exception)
    {
        $this->get('logger')->error($exception->getMessage(), ['exception' => $exception]);
    }

    /**
     * @param $message
     */
    protected function addSuccessMessage($message)
    {
        $this->get('session')->getFlashBag()->add(self::MESSAGE_SUCCESS_TYPE, $message);
    }

    /**
     * @param $message
     */
    protected function addInformationMessage($message)
    {
        $this->get('session')->getFlashBag()->add(self::MESSAGE_INFORMATION_TYPE, $message);
    }

    /**
     * @param $message
     */
    protected function addWarningMessage($message)
    {
        $this->get('session')->getFlashBag()->add(self::MESSAGE_WARNING_TYPE, $message);
    }

    /**
     * @param $message
     */
    protected function addErrorMessage($message)
    {
        $this->get('session')->getFlashBag()->add(self::MESSAGE_DANGER_TYPE, $message);
    }

    /**
     * @param FormInterface $form
     */
    protected function addFormErrorsFlashes(FormInterface $form)
    {
        foreach ($form->getErrors() as $error) {
            $this->addErrorMessage($error->getMessage());
        }
    }
}
