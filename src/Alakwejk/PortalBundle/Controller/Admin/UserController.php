<?php

namespace Alakwejk\PortalBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Alakwejk\PortalBundle\Controller\BaseController;
use Alakwejk\PortalBundle\Entity\User;
use Alakwejk\PortalBundle\Form\UserType;
use FOS\UserBundle\Model\UserInterface;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class UserController extends BaseController
{
    /**
     * @Route("/user", name="alakwejk_admin_user_list")
     *
     * @Method({"GET"})
     * @Template(":Admin/User:list.html.twig")
     */
    public function listAction(Request $request)
    {
        return [
            'users' => $this->get('bilety.user.repository')->findAll(),
        ];
    }

    /**
     * @Route("/user/new", name="alakwejk_admin_user_create")
     *
     * @Method({"GET", "POST"})
     * @Template(":Admin/User:create.html.twig")
     */
    public function createAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new UserType(), $user, [
            'action' => $this->generateUrl('alakwejk_admin_user_create'),
            'validation_groups' => ['Profile'],
            'method' => 'POST',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('bilety.user.saver')->save($user);
            $this->addSuccessMessage('Stworzono użytkownika');

            return $this->redirectToRoute('alakwejk_admin_user_list');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/user/{id}", name="alakwejk_admin_user_edit")
     * @ParamConverter("user", class="AlakwejkPortalBundle:User", options={"mapping": {"id": "id"}})
     *
     * @Method({"GET", "PUT"})
     * @Template(":Admin/User:edit.html.twig")
     */
    public function editAction(Request $request, User $user)
    {
        $editFormView = $this->createForm(new UserType(), $user, [
            'action' => $this->generateUrl('alakwejk_admin_user_edit', [
                'id' => $user->getId(),
            ]),
            'validation_groups' => ['Default', 'Profile'],
            'method' => 'PUT',
        ]);

        $editFormView->handleRequest($request);
        if ($editFormView->isSubmitted() && $editFormView->isValid()) {
            $this->get('bilety.user.saver')->save($user);
            $this->addSuccessMessage('Zapisano zmiany użytkownika');

            return $this->redirectToRoute('alakwejk_admin_user_list');
        }

        return [
            'user' => $user,
            'form' => $editFormView->createView(),
            'deleteForm' => $this->createDeleteForm($user)->createView(),
        ];
    }

    /**
     * @Route("/user/{id}/activate", name="alakwejk_admin_user_activate")
     * @ParamConverter("user", class="AlakwejkPortalBundle:User", options={"mapping": {"id": "id"}})
     *
     * @Method({"GET"})
     */
    public function activateAction(Request $request, User $user)
    {
        /** @var $user User */
        /** @var $user UserInterface */
        if ($user->isEnabled()) {
            $this->get('bilety.user.manager')->deactivate($user);
            $this->addWarningMessage(sprintf('Deaktywowano użytkownika %s.', $user->getFullName()));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('alakwejk_admin_user_list');
        }

        try {
            $this->get('bilety.user.manager')->activate($user);
            $this->getDoctrine()->getManager()->flush();
            $this->addSuccessMessage(
                sprintf(
                    'Aktywowano użytkownika %s. Na adres %s wysłano email z hasłem.',
                    $user->getFullName(),
                    $user->getEmail()
                )
            );
        } catch (\Exception $exception) {
            $this->addErrorMessage(
                sprintf(
                    'Wystąpił błąd podczas aktywacji użytkownika %s',
                    $user->getFullName()
                )
            );

            $this->logException($exception);
        }

        return $this->redirectToRoute('alakwejk_admin_user_list');
    }

    /**
     * @Route("/user/{id}", name="alakwejk_admin_user_delete")
     * @ParamConverter("user", class="AlakwejkPortalBundle:User", options={"mapping": {"id": "id"}})
     *
     * @Method({"DELETE"})
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            $this->addWarningMessage('Usunięto użytkownika');
        }

        return $this->redirect($this->generateUrl('alakwejk_admin_user_list'));
    }

    /**
     * @param User $user
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('alakwejk_admin_user_delete', ['id' => $user->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
