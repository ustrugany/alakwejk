<?php

namespace Alakwejk\PortalBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Alakwejk\PortalBundle\Entity\Post;
use Alakwejk\PortalBundle\Post\Exception\PostSaverException;
use Alakwejk\PortalBundle\Controller\BaseController;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class PostController extends BaseController
{
    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/post", name="alakwejk_admin_post_list")
     *
     * @Method({"GET"})
     * @Template(":Admin/Post:list.html.twig")
     */
    public function indexAction()
    {
        $posts = $this->getPostRepository()->findAll();

        return [
            'events' => $posts,
        ];
    }

    /**
     * @Route("/post/new", name="alakwejk_admin_post_create")
     *
     * @Method({"GET", "POST"})
     * @Template(":Admin/Post:create.html.twig")
     */
    public function createAction(Request $request)
    {
        $post = (new Post())->setUser($this->getUser());
        $form = $this->createForm('post_type', $post, [
            'action' => $this->generateUrl('alakwejk_admin_post_create'),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            try {
                $this->get('bilety.post.saver')->save($post);
            } catch (PostSaverException $e) {
                $this->addErrorMessage($e->getMessage());

                return $this->redirectToRoute('alakwejk_admin_post_edit', ['id' => $post->getId()]);
            }

            $this->addSuccessMessage('Wpis utworzony');

            return $this->redirectToRoute('alakwejk_admin_post_edit', ['id' => $post->getId()]);
        }

        return [
            'form' => $form->createView(),
            'event' => $post,
        ];
    }

    /**
     * @Security("is_granted('EDIT', event)")
     * @Route("/post/{id}", name="alakwejk_admin_post_edit")
     * @ParamConverter("post", class="AlakwejkPortalBundle:Post", options={"mapping": {"id": "id"}})
     *
     * @Method({"GET"})
     * @Template(":Admin/Post:edit.html.twig")
     */
    public function editAction(Request $request, Post $post)
    {
        $form = $this->createForm('post_type', $post, [
            'action' => $this->generateUrl('alakwejk_admin_post_update', ['id' => $post->getId()]),
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        $deleteForm = $this->createSimpleForm('DELETE',
            $this->generateUrl('alakwejk_admin_post_delete', ['id' => $post->getId()]));

        return [
            'form' => $form->createView(),
            'deleteForm' => $deleteForm->createView(),
            'event' => $post,
        ];
    }

    /**
     * @Security("is_granted('EDIT', event)")
     * @Route("/post/{id}", name="alakwejk_admin_post_update")
     * @ParamConverter("post", class="AlakwejkPortalBundle:Post", options={"mapping": {"id": "id"}})
     *
     * @Method({"PUT"})
     * @Template(":Admin/Post:edit.html.twig")
     */
    public function updateAction(Request $request, Post $post)
    {
        $form = $this->createForm('post_type', $post, [
            'action' => $this->generateUrl('alakwejk_admin_post_update', ['id' => $post->getId()]),
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            try {
                $this->get('bilety.post.saver')->save($post);
            } catch (PostSaverException $e) {
                $this->addErrorMessage($e->getMessage());

                return $this->redirectToRoute('alakwejk_admin_post_edit', ['id' => $post->getId()]);
            }

            $this->addSuccessMessage('Post wyedytowany');

            return $this->redirectToRoute('alakwejk_admin_post_edit', ['id' => $post->getId()]);
        }

        $deleteForm = $this->createSimpleForm('DELETE',
            $this->generateUrl('alakwejk_admin_post_delete', ['id' => $post->getId()])
        );

        return [
            'form' => $form->createView(),
            'deleteForm' => $deleteForm->createView(),
            'event' => $post,
        ];
    }

    /**
     * @Security("is_granted('DELETE', event)")
     * @Route("/post/{id}", name="alakwejk_admin_post_delete")
     * @ParamConverter("event", class="AlakwejkPortalBundle:Post", options={"mapping": {"id": "id"}})
     *
     * @Method({"DELETE"})
     */
    public function deleteAction(Request $request, Post $post)
    {
        $form = $this->createSimpleForm('DELETE',
            $this->generateUrl('alakwejk_admin_post_delete', ['id' => $post->getId()]));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush();

            $this->addWarningMessage('Post usunięty');
        }

        return $this->redirect($this->generateUrl('alakwejk_admin_post_list'));
    }

    /**
     * @param $method
     * @param $action
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createSimpleForm($method, $action)
    {
        return $this->createFormBuilder()
            ->setAction($action)
            ->setMethod($method)
            ->getForm();
    }
}
