<?php

namespace Alakwejk\PortalBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Alakwejk\PortalBundle\Controller\BaseController;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class DashboardController extends BaseController
{
    /**
     * @Route("/", name="alakwejk_admin_dashboard")
     *
     * @Method({"GET"})
     * @Template(":Admin:layout.html.twig")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('alakwejk_admin_Post_list');
    }
}
