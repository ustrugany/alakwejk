<?php

namespace Alakwejk\PortalBundle\Controller;

use Alakwejk\PortalBundle\Form\ProfileType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_USER')")
 */
class ProfileController extends BaseController
{
    /**
     * @Route("/profile", name="alakwejk_profile")
     * @Method({"GET", "PUT"})
     * @Template(":Profile:edit.html.twig")
     */
    public function editAction(Request $request)
    {
        $editFormView = $this->createForm(new ProfileType(), $this->getUser(), [
            'action' => $this->generateUrl('alakwejk_admin_profile_edit'),
            'validation_groups' => ['Default', 'Profile'],
            'method' => 'PUT',
        ]);

        $editFormView->handleRequest($request);
        if ($editFormView->isSubmitted() && $editFormView->isValid()) {

            $this->get('alakwejk.user.saver')->save($this->getUser());
            $this->addSuccessMessage('Zapisano zmiany użytkownika');

            return $this->redirectToRoute('alakwejk_admin_dashboard');
        }

        return [
            'user' => $this->getUser(),
            'form' => $editFormView->createView(),
        ];
    }
}
