<?php
namespace Alakwejk\PortalBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture,
    Doctrine\Common\DataFixtures\OrderedFixtureInterface,
    Doctrine\Common\Persistence\ObjectManager,
    Symfony\Component\DependencyInjection\ContainerAwareInterface,
    Symfony\Component\DependencyInjection\ContainerInterface,
    Symfony\Component\Yaml\Yaml;

use Alakwejk\PortalBundle\Entity\Vote;

class LoadVoteData extends AbstractFixture implements OrderedFixtureInterface,
    ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $posts = Yaml::parse(file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'data/votes.yml'));

        foreach ($posts as $data) {
            $vote = new Vote();
            $vote->setValue($data['value']);
            $this->getManager()->persist($vote);

            if (!$this->referenceRepository->hasReference($data['user'])) {
                continue;
            }

            $user = $manager->merge($this->referenceRepository->getReference($data['user']));
            $vote->setUser($user);

            if (!$this->referenceRepository->hasReference($data['post'])) {
                continue;
            }

            $post = $manager->merge($this->referenceRepository->getReference($data['post']));
            $vote->setPost($post);

            if (!$this->referenceRepository->hasReference($data['reference'])) {
                $this->referenceRepository->addReference($data['reference'], $vote);
            }

        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    private function getManager()
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }
}
