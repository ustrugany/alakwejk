<?php

namespace Alakwejk\PortalBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Alakwejk\PortalBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface,
    ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $users = Yaml::parse(file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'data/users.yml'));

        foreach ($users as $data) {
            $user = new User();
            $user->setEmail($data['email']);
            $user->setUsername($data['email']);
            $user->setPlainPassword($data['password']);
            $user->setRoles($data['roles']);
            $user->setEnabled($data['enabled']);

            $this->getFosUserManager()->updateUser($user);

            if (!$this->referenceRepository->hasReference($data['reference'])) {
                $this->referenceRepository->addReference($data['reference'], $user);
            }
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * @return \FOS\UserBundle\Doctrine\UserManager
     */
    private function getFosUserManager()
    {
        return $this->container->get('fos_user.user_manager');
    }
}
