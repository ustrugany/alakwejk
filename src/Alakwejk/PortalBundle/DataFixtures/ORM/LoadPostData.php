<?php

namespace Alakwejk\PortalBundle\DataFixtures\ORM;

use Alakwejk\PortalBundle\Entity\Image;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;
use Alakwejk\PortalBundle\Entity\Post;

class LoadPostData extends AbstractFixture implements OrderedFixtureInterface,
    ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $posts = Yaml::parse(file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'data/posts.yml'));

        foreach ($posts as $data) {
            $post = new Post();
            $post->setTitle($data['title']);
            $post->setDescription($data['description']);
            $post->setPublished($data['published']);
            $this->getManager()->persist($post);

            if (!$this->referenceRepository->hasReference($data['user'])) {
                continue;
            }

            $user = $manager->merge($this->referenceRepository->getReference($data['user']));
            $post->setUser($user);


            $path = $this->container->getParameter('web_directory_path') . '/assets/img/';
            $imageCopyPath = $path .'demot.copy.jpg';
            copy($path . 'demot.jpg', $imageCopyPath);

            $image = (new Image)->setFile(new UploadedFile(
                $imageCopyPath,
                'Image1', null, null, null, true
            ));

            $post->setImage($image);
            $this->container->get('alakwejk.image.saver')->save($post->getImage());

            if (!$this->referenceRepository->hasReference($data['reference'])) {
                $this->referenceRepository->addReference($data['reference'], $post);
            }
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    private function getManager()
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }
}
