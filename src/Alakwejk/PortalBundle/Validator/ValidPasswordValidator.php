<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Validator;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use FOS\UserBundle\Model\UserInterface;
use Alakwejk\PortalBundle\Model\ChangePassword;

class ValidPasswordValidator extends ConstraintValidator
{
    private $tokenStorage;

    private $encoderFactory;

    public function __construct(TokenStorage $tokenStorage, EncoderFactory $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
        $this->tokenStorage = $tokenStorage;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$value instanceof ChangePassword) {
            return;
        }

        $user = $this->getUser();
        if (!$user) {
            return;
        }

        $user->setPlainPassword($value->getPassword());
        if (!$this->isPasswordValid($user)) {
            $this->context->buildViolation($constraint->message)
                ->atPath('password')
                ->addViolation();
        }
    }

    /**
     * @param UserInterface $user
     *
     * @return bool
     */
    private function isPasswordValid(UserInterface $user)
    {
        $encoder = $this->encoderFactory->getEncoder($user);

        return $encoder->isPasswordValid($user->getPassword(), $user->getPlainPassword(), $user->getSalt());
    }

    /**
     * @return UserInterface|null
     */
    private function getUser()
    {
        if ($this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser() instanceof UserInterface) {
            return $this->tokenStorage->getToken()->getUser();
        }

        return;
    }
}
