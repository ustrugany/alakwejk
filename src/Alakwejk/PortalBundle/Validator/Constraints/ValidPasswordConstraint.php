<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidPasswordConstraint extends Constraint
{
    public $message = 'Podane hasło nie jest poprawne';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'valid_password_validator';
    }

    /**
     * @return string
     */
    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}
