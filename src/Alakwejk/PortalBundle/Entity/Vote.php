<?php
/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */

namespace Alakwejk\PortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM,
    Symfony\Component\Validator\Constraints as Assert;

use Alakwejk\Portal\Post\PostInterface,
    Alakwejk\Portal\User\UserInterface;


/**
 * @ORM\Table(name="vote")
 * @ORM\Entity(repositoryClass="Alakwejk\PortalBundle\Repository\VoteRepository")
 */
class Vote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @Assert\NotNull()
     * @Assert\Choice(choices = {-1, 1}, message = "Poprawne wartosci to -1 lub 1", strict = true)
     * @ORM\Column(name="value", type="smallint")
     */
    private $value;

    /**
     * @var User
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Alakwejk\PortalBundle\Entity\Post", inversedBy="votes")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $post;

    /**
     * @var User
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Alakwejk\PortalBundle\Entity\User", inversedBy="votes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value int
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return PostInterface
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param PostInterface $post
     * @return $this
     */
    public function setPost(PostInterface $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     * @return $this
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;

        return $this;
    }
}