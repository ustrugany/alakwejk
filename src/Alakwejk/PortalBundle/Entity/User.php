<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Entity\User as BaseUser;
use Alakwejk\Portal\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Alakwejk\PortalBundle\Repository\UserRepository")
 * @UniqueEntity(
 *      fields={"email"},
 *      message="Ten email jest już zajęty."
 * )
 */
class User extends BaseUser implements UserInterface
{
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER = 'ROLE_USER';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Email(
     *     message = "Niepoprawny adres email.",
     *     checkMX = true
     * )
     * @Assert\NotNull(message="Pole wymagane")
     */
    protected $email;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *                      targetEntity="Alakwejk\PortalBundle\Entity\Post",
     *                      mappedBy="user",
     *                      cascade={"persist"}
     *                      )
     **/
    private $posts;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *      targetEntity="Alakwejk\PortalBundle\Entity\Vote",
     *      mappedBy="user",
     *      cascade={"persist", "remove"}
     * )
     **/
    private $votes;

    public function __construct()
    {
        parent::__construct();

        $this->posts = new ArrayCollection();
        $this->votes = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param ArrayCollection $positions
     *
     * @return $this
     */
    public function setPosts(ArrayCollection $positions)
    {
        $this->posts = $positions;

        return $this;
    }

    /**
     * @param Post $event
     *
     * @return $this
     */
    public function addPost(Post $event)
    {
        if (!$this->posts->contains($event)) {
            $this->posts->add($event);
        }

        return $this;
    }

    /**
     * @return Vote[]
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * @param Vote[] $votes
     *
     * @return $this
     */
    public function setVotes(ArrayCollection $votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * @param Vote $vote
     * @return $this
     */
    public function addVote(Vote $vote)
    {
        if (!$this->votes->contains($vote)) {
            $this->votes->add($vote);
        }

        return $this;
    }
}
