<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Alakwejk\Portal\Upload\Document\DocumentInterface;

/**
 * @ORM\Entity()
 */
class Document extends Upload implements DocumentInterface
{
    /**
     * @Assert\File(
     *     maxSize = "4M",
     *     mimeTypes = {
     *          "application/pdf",
     *          "application/msword",
     *          "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
     *      },
     *     maxSizeMessage = "Plik jest za duży. Dozwolone są pliki poniżej 4MB.",
     * )
     */
    protected $file;
}
