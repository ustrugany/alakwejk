<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Alakwejk\Portal\Upload\Upload as BaseUpload;
use Alakwejk\Portal\Upload\UploadInterface;

/**
 * @ORM\Table(name="upload")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"image" = "Alakwejk\PortalBundle\Entity\Image", "document" = "Alakwejk\PortalBundle\Entity\Document"} )
 */
class Upload extends BaseUpload implements UploadInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $mime;

    /**
     * @var string
     * @ORM\Column(name="original_name", type="string", length=255, nullable=false)
     */
    protected $originalName;

    /**
     * @var \DateTime
     * @ORM\Column(name="uploaded_at", type="datetime")
     */
    protected $uploadedAt;

    /**
     * @Assert\File(
     *     maxSize = "5M",
     *     maxSizeMessage = "Dozwolne są pliki do 5MB"
     * )
     */
    protected $file;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets file.
     *
     * @param \SplFileInfo $file
     */
    public function setFile(\SplFileInfo $file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return \SplFileInfo
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return \DateTime
     */
    public function getUploadedAt()
    {
        return $this->uploadedAt;
    }

    /**
     * @param \DateTime $uploadedAt
     */
    public function setUploadedAt(\DateTime $uploadedAt)
    {
        $this->uploadedAt = $uploadedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->uploadedAt = new \DateTime();
    }
}
