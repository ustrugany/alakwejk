<?php

namespace Alakwejk\PortalBundle\Entity;

use Alakwejk\Portal\Post\PostInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Alakwejk\Portal\Upload\UploadInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="Alakwejk\PortalBundle\Repository\PostRepository")
 */
class Post implements PostInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull()
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var bool
     *
     * @ORM\Column(name="published", type="boolean")
     */
    private $published;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     * @Assert\NotNull()
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Alakwejk\PortalBundle\Entity\User", inversedBy="posts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @var UploadInterface
     * @Assert\Valid()
     * @ORM\OneToOne(targetEntity="Alakwejk\PortalBundle\Entity\Image")
     */
    private $image;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *      targetEntity="Alakwejk\PortalBundle\Entity\Vote",
     *      mappedBy="user",
     *      cascade={"persist", "remove"}
     * )
     **/
    private $votes;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->published = false;
        $this->votes = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set published.
     *
     * @param bool $published
     *
     * @return Post
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published.
     *
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Post
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return UploadInterface
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param UploadInterface $image
     *
     * @return $this
     */
    public function setImage(UploadInterface $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return UploadInterface
     */
    public function getUpload()
    {
        return $this->getImage();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Vote[]
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * @param Vote[] $votes
     * @return $this
     */
    public function setVotes(ArrayCollection $votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * @param Vote $vote
     * @return $this
     */
    public function addVote(Vote $vote)
    {
        if (!$this->votes->contains($vote)) {
            $this->votes->add($vote);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getRate()
    {
        /**
         * @todo Requires implementation
         */
        return 100;
    }
}