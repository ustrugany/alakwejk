<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Alakwejk\Portal\Upload\Image\ImageInterface;

/**
 * @ORM\Entity()
 */
class Image extends Upload implements ImageInterface
{
    /**
     * @ORM\Column(name="width", type="integer", nullable=false)
     */
    protected $width;

    /**
     * @ORM\Column(name="height", type="integer", nullable=false)
     */
    protected $height;

    /**
     * @Assert\File(
     *     maxSize = "4M",
     *     mimeTypes = {"image/png", "image/jpeg"},
     *     maxSizeMessage = "Plik jest za duży. Dozwolone są pliki poniżej 4MB.",
     * )
     * @Assert\Image(
     *     minWidth = 180,
     *     maxWidth = 1024,
     *     minHeight = 240,
     *     maxHeight = 768,
     *     allowLandscape = true,
     *     allowPortrait = true
     * )
     */
    protected $file;

    /**
     * @param int $height
     *
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $width
     *
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }
}
