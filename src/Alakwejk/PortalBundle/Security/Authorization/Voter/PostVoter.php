<?php

/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace Alakwejk\PortalBundle\Security\Authorization\Voter;

use Alakwejk\PortalBundle\Entity\Post;
use Alakwejk\PortalBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class PostVoter implements VoterInterface
{
    const EDIT = 'EDIT';
    const DELETE = 'DELETE';
    const SUPPORTED_CLASS = 'Alakwejk\PortalBundle\Entity\Post';

    /**
     * @param string $attribute
     *
     * @return bool
     */
    public function supportsAttribute($attribute)
    {
        return in_array($attribute, array(
            self::EDIT,
            self::DELETE,
        ));
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return self::SUPPORTED_CLASS === $class || is_subclass_of($class, self::SUPPORTED_CLASS);
    }

    /**
     * @param TokenInterface $token
     * @param Post           $post
     * @param array          $attributes
     *
     * @return int
     */
    public function vote(TokenInterface $token, $post, array $attributes)
    {
        if (!$this->supportsClass(get_class($post))) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        if (1 !== count($attributes)) {
            throw new \InvalidArgumentException(
                'Only one attribute is allowed for VIEW or EDIT'
            );
        }

        $attribute = $attributes[0];
        if (!$this->supportsAttribute($attribute)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return VoterInterface::ACCESS_DENIED;
        }

        if (in_array(User::ROLE_SUPER_ADMIN, $user->getRoles())) {
            return VoterInterface::ACCESS_GRANTED;
        }

        switch ($attribute) {
            case self::EDIT:
                if ($user->getId() === $post->getUser()->getId()) {
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
            case self::DELETE:
                if ($user->getId() === $post->getUser()->getId()) {
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
        }

        return VoterInterface::ACCESS_DENIED;
    }
}
