<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Model;

class Menu
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $path;

    /**
     * @var bool
     */
    private $active;

    /**
     * @param $name
     * @param $path
     * @param $active
     */
    public function __construct($name, $path, $active)
    {
        $this->name = $name;
        $this->path = $path;
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }
}
