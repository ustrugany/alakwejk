<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Upload;

use Alakwejk\PortalBundle\Upload\Exception\UploadSaverException;
use Alakwejk\Portal\Upload\UploadInterface;
use Alakwejk\Portal\Upload\UploadSaverInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadSaver implements UploadSaverInterface
{
    /**
     * @var string
     */
    protected $uploadsDirectory;

    /**
     * @var string
     */
    protected $webDirectoryPath;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param $webDirectoryPath
     * @param $uploadsDirectory
     * @param EntityManager $entityManager
     */
    public function __construct($webDirectoryPath, $uploadsDirectory, EntityManager $entityManager)
    {
        $this->webDirectoryPath = $webDirectoryPath;
        $this->uploadsDirectory = $uploadsDirectory;
        $this->entityManager = $entityManager;
    }

    /**
     * @param UploadInterface $upload
     *
     * @return $upload
     */
    public function save(UploadInterface $upload)
    {
        try {
            /** @var $file UploadedFile */
            $file = $upload->getFile();
            $upload->setOriginalName($file->getClientOriginalName());
            $upload->setMime($file->getMimeType());
            $upload->setName($this->generateName().'.'.$file->guessExtension());
            $this->upload($upload);
        } catch (FileException $e) {
            throw (new UploadSaverException($e->getMessage(), null, $e))
                ->setUpload($upload);
        }

        try {
            $this->entityManager->persist($upload);
            $this->entityManager->flush($upload);
        } catch (ORMException $e) {
            $this->removeUpload($upload);

            throw (new UploadSaverException($e->getMessage(), null, $e))
                ->setUpload($upload);
        }

        return $upload;
    }

    /**
     * @param UploadInterface $upload
     */
    public function removeUpload(UploadInterface $upload)
    {
        $file = $this->getUploadPath($upload);
        if ($file) {
            unlink($file);
        }
    }

    /**
     * @param UploadInterface $upload
     */
    protected function upload(UploadInterface $upload)
    {
        /** @var UploadedFile $file */
        $file = $upload->getFile();
        $file->move($this->getUploadsDirectoryPath(), $upload->getName());
    }

    /**
     * @param UploadInterface $upload
     *
     * @return string
     */
    protected function getUploadPath(UploadInterface $upload)
    {
        return $this->getUploadsDirectoryPath().'/'.$upload->getName();
    }

    /**
     * @return string
     */
    protected function getUploadsDirectoryPath()
    {
        return $this->webDirectoryPath.$this->getUploadsDirectory();
    }

    /**
     * @return string
     */
    protected function getUploadsDirectory()
    {
        return $this->uploadsDirectory;
    }

    /**
     * @return string
     */
    protected function generateName()
    {
        return sha1(uniqid(mt_rand(), true));
    }
}
