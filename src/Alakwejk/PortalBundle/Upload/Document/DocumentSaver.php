<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Upload\Document;

use Alakwejk\PortalBundle\Upload\UploadSaver;
use Alakwejk\Portal\Upload\UploadInterface;

class DocumentSaver extends UploadSaver
{
    /**
     * @param UploadInterface $upload
     */
    public function save(UploadInterface $upload)
    {
        return parent::save($upload);
    }
}
