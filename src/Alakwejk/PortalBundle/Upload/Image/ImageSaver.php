<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Upload\Image;

use Alakwejk\PortalBundle\Entity\Image;
use Alakwejk\PortalBundle\Upload\Exception\Image\ImageSaverException;
use Alakwejk\PortalBundle\Upload\UploadSaver;
use Alakwejk\Portal\Upload\Image\ImageSaverInterface;
use Alakwejk\Portal\Upload\UploadInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\Filesystem\Filesystem;

class ImageSaver extends UploadSaver implements ImageSaverInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param UploadInterface $upload
     *
     * @return Image|UploadInterface
     */
    public function save(UploadInterface $upload)
    {
        /* @var Image $upload */
        parent::save($upload);
        if ($this->filesystem->exists($this->getUploadPath($upload))) {
            list($width, $height) = getimagesize($this->getUploadPath($upload));
            $upload->setHeight($height);
            $upload->setWidth($width);

            try {
                $this->entityManager->flush($upload);
            } catch (ORMException $e) {
                $this->removeUpload($upload);
                throw (new ImageSaverException('Błąd podczas zapisywania danych pliku graficznego.', null, $e))
                    ->setUpload($upload);
            }

            return $upload;
        } else {
            throw (new ImageSaverException('Błąd podczas zapisu pliku graficznego. Plik nie istnieje!'))
                ->setUpload($upload);
        }
    }

    /**
     * @param Filesystem $filesystem
     */
    public function setFilesystem(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }
}
