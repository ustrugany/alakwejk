<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Upload\Exception;

use Alakwejk\Portal\Upload\Exception\UploadSaverExceptionInterface;
use Alakwejk\Portal\Upload\UploadInterface;

class UploadSaverException extends \RuntimeException implements UploadSaverExceptionInterface
{
    /**
     * @var UploadInterface
     */
    private $upload;

    /**
     * @return UploadInterface
     */
    public function getUpload()
    {
        return $this->upload;
    }

    /**
     * @param UploadInterface $upload
     *
     * @return $this
     */
    public function setUpload(UploadInterface $upload)
    {
        $this->upload = $upload;

        return $this;
    }
}
