<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Upload\Exception\Image;

use Alakwejk\PortalBundle\Upload\Exception\UploadSaverException;

class ImageSaverException extends UploadSaverException
{
}
