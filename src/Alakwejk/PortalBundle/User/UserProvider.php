<?php

/**
 * @author DtS
 */
namespace Alakwejk\PortalBundle\User;

use Alakwejk\Portal\User\UserInterface;
use Alakwejk\PortalBundle\Repository\UserRepository;

/**
 * Class UserProvider.
 *
 * @todo zmienic tak, aby pobieral usera z security context
 */
class UserProvider
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->userRepository->findOneBy([
            'username' => 'user01@test.pl',
        ]);
    }
}
