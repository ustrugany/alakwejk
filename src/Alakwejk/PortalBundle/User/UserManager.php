<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\User;

use FOS\UserBundle\Entity\User;
use FOS\UserBundle\Doctrine\UserManager as FosUserManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Bundle\TwigBundle\TwigEngine;

class UserManager implements UserManagerInterface
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var FosUserManager
     */
    private $fosUserManager;

    /**
     * @var TwigEngine
     */
    private $twigEngine;

    /**
     * @var string
     */
    private $contactEmail;

    /**
     * @var \FOS\UserBundle\Util\TokenGeneratorInterface
     */
    private $generator;

    public function __construct(
        \Swift_Mailer $mailer,
        FosUserManager $fosUserManager,
        TwigEngine $twigEngine,
        TokenGenerator $generator,
        $contactEmail
    ) {
        $this->generator = $generator;
        $this->mailer = $mailer;
        $this->twigEngine = $twigEngine;
        $this->fosUserManager = $fosUserManager;
        $this->contactEmail = $contactEmail;
    }

    /**
     * @param UserInterface $user
     */
    public function activate(UserInterface $user)
    {
        $password = substr($this->generator->generateToken(), 0, 10);
        $user->setEnabled(true);
        $user->setPlainPassword($password);
        $this->fosUserManager->updateUser($user);

        $message = $this->mailer->createMessage()
            ->setSubject('Aktywowano konto!')
            ->setFrom($this->contactEmail)
            ->setTo($user->getEmail())
            ->setBody(
                $this->twigEngine->render(
                    ':User:activatedEmail.html.twig',
                    [
                        'user' => $user,
                        'password' => $password,
                    ]
                ),
                'text/html'
            );
        $this->mailer->send($message);
    }

    /**
     * @param UserInterface $user
     */
    public function deactivate(UserInterface $user)
    {
        /* @var $user User */
        /* @var $user UserInterface */
        $user->setEnabled(false);

        $message = $this->mailer->createMessage()
            ->setSubject('Deaktywowano konto!')
            ->setFrom($this->contactEmail)
            ->setTo($user->getEmail())
            ->setBody(
                $this->twigEngine->render(':User:deactivatedEmail.html.twig'),
                'text/html'
            );

        $this->mailer->send($message);
    }
}
