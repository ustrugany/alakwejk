<?php

/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace Alakwejk\PortalBundle\User;

use Alakwejk\Portal\User\UserInterface;

interface UserSaverInterface
{
    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function save(UserInterface $user);
}
