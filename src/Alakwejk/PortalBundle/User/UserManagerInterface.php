<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\User;

use FOS\UserBundle\Model\UserInterface;

interface UserManagerInterface
{
    /**
     * @param UserInterface $user
     */
    public function activate(UserInterface $user);

    /**
     * @param UserInterface $user
     */
    public function deactivate(UserInterface $user);
}
