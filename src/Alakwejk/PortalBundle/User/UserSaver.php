<?php

/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace Alakwejk\PortalBundle\User;

use Alakwejk\PortalBundle\Upload\Document\DocumentSaver;
use Alakwejk\PortalBundle\Upload\Image\ImageSaver;
use Alakwejk\Portal\Upload\Document\HasDocumentGuard;
use Alakwejk\Portal\Upload\Document\HasDocumentGuardInterface;
use Alakwejk\Portal\Upload\Document\HasDocumentInterface;
use Alakwejk\Portal\Upload\Image\HasImageGuard;
use Alakwejk\Portal\Upload\Image\HasImageGuardInterface;
use Alakwejk\Portal\Upload\Image\HasImageInterface;
use Alakwejk\Portal\User\UserInterface;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;

class UserSaver implements UserSaverInterface
{
    /**
     * @var DocumentSaver
     */
    private $documentSaver;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var HasDocumentGuardInterface
     */
    private $documentGuard;

    /**
     * @var HasImageGuardInterface
     */
    private $imageGuard;

    /**
     * @var UserManager
     */
    private $fosUserManager;

    /**
     * @param DocumentSaver $documentSaver
     * @param ImageSaver    $imageSaver
     * @param EntityManager $entityManager
     * @param UserManager   $fosUserManager
     */
    public function __construct(
        DocumentSaver $documentSaver,
        ImageSaver $imageSaver,
        EntityManager $entityManager,
        UserManager $fosUserManager
    ) {
        $this->documentSaver = $documentSaver;
        $this->imageSaver = $imageSaver;
        $this->entityManager = $entityManager;
        $this->documentGuard = new HasDocumentGuard();
        $this->imageGuard = new HasImageGuard();
        $this->fosUserManager = $fosUserManager;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function save(UserInterface $user)
    {
        /** @var $user HasDocumentInterface */
        if ($this->documentGuard->isUploadValid($user)) {
            $this->documentSaver->save($user->getDocument());
        }

        /** @var $user HasImageInterface */
        if ($this->imageGuard->isUploadValid($user)) {
            $this->imageSaver->save($user->getImage());
        }

        $this->fosUserManager->updateUser($user, true);
    }
}
