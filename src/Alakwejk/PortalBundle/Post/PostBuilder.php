<?php

/**
 * @author DtS
 */
namespace Alakwejk\PortalBundle\Post;

use Alakwejk\Portal\User\UserInterface;
use Alakwejk\PortalBundle\Entity\Post;

/**
 * Class PostBuilder.
 */
class PostBuilder implements PostBuilderInterface
{
    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * @inheritdoc
     */
    public function create()
    {
        $post = new Post();
        $post->setUser($this->user);

        return $post;
    }
}
