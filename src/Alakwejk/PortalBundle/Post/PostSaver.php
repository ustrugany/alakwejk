<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Post;

use Alakwejk\PortalBundle\Post\Exception\PostSaverException;
use Alakwejk\PortalBundle\Upload\Exception\UploadSaverException;
use Alakwejk\Portal\Post\PostInterface;
use Alakwejk\Portal\Post\PostSaverInterface;
use Alakwejk\Portal\Upload\Image\HasImageGuard;
use Alakwejk\Portal\Upload\UploadSaverInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

class PostSaver implements PostSaverInterface
{
    /**
     * @var UploadSaverInterface
     */
    private $imageSaver;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var HasImageGuard
     */
    private $imageGuard;

    /**
     * @param UploadSaverInterface $uploadSaver
     * @param EntityManager        $entityManager
     */
    public function __construct(
        UploadSaverInterface $uploadSaver,
        EntityManager $entityManager
    ) {
        $this->entityManager = $entityManager;
        $this->imageSaver = $uploadSaver;
        $this->imageGuard = new HasImageGuard();
    }

    /**
     * @param PostInterface $event
     *
     * @throws PostSaverException
     */
    public function save(PostInterface $event)
    {
        try {
            if ($this->imageGuard->isUploadValid($event)) {
                $this->imageSaver->save($event->getImage());
            }

            $this->entityManager->persist($event);
            $this->entityManager->flush();
        } catch (UploadSaverException $e) {
            throw (new PostSaverException($e->getMessage(), null, $e))
                ->setPost($event);
        } catch (ORMException $e) {
            throw (new PostSaverException($e->getMessage(), null, $e))
                ->setPost($event);
        }
    }
}
