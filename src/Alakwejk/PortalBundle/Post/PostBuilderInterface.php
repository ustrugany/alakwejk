<?php

/**
 * @author DtS
 */
namespace Alakwejk\PortalBundle\Post;

use Alakwejk\Portal\Post\PostInterface;

interface PostBuilderInterface
{
    /**
     * @return PostInterface
     */
    public function create();
}
