<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\PortalBundle\Post\Exception;

use Alakwejk\Portal\Post\PostInterface;
use Alakwejk\Portal\Post\Exception\PostSaverExceptionInterface;

class PostSaverException extends \RuntimeException implements PostSaverExceptionInterface
{
    /**
     * @var PostInterface
     */
    private $post;

    /**
     * @return PostInterface
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param PostInterface $post
     *
     * @return $this
     */
    public function setPost(PostInterface $post)
    {
        $this->post = $post;

        return $this;
    }
}
