<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Post\Exception;

use Alakwejk\Portal\Post\PostInterface;

interface PostSaverExceptionInterface
{
    /**
     * @return PostInterface
     */
    public function getPost();
}
