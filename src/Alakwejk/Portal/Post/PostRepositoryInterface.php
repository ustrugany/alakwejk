<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Alakwejk\Portal\Post;

use Alakwejk\PortalBundle\Entity\Post;
use Alakwejk\Portal\User\UserInterface;

interface PostRepositoryInterface
{
    /**
     * @return Post[]
     */
    public function findPublishedOrderedByCreatedAt();

    /**
     * @return Post[]
     */
    public function findUsersPublishedOrderedByCreatedAt(UserInterface $user);
}
