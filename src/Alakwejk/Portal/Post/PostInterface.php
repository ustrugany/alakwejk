<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Post;

use Alakwejk\Portal\Upload\Image\HasImageInterface;
use Alakwejk\Portal\User\UserInterface;

interface PostInterface extends HasImageInterface
{
    /**
     * Get id.
     *
     * @return int
     */
    public function getId();

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle();

    /**
     * Get published.
     *
     * @return bool
     */
    public function isPublished();

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription();

    /**
     * @return UserInterface
     */
    public function getUser();

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @return int
     */
    public function getRate();
}