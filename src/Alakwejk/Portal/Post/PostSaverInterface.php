<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Post;

interface PostSaverInterface
{
    /**
     * @param PostInterface $post
     *
     * @return PostInterface
     */
    public function save(PostInterface $post);
}
