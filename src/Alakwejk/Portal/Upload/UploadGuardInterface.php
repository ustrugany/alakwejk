<?php

/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace Alakwejk\Portal\Upload;

interface UploadGuardInterface
{
    /**
     * @param UploadInterface $upload
     *
     * @return bool
     */
    public function isUploadValid(UploadInterface $upload);
}
