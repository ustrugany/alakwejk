<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Upload;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface UploadInterface
{
    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getOriginalName();

    /**
     * @return string
     */
    public function getMime();
}
