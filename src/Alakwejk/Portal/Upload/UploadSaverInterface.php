<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Upload;

interface UploadSaverInterface
{
    /**
     * @param UploadInterface $upload
     *
     * @return $upload
     */
    public function save(UploadInterface $upload);
}
