<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Upload\Exception;

use Alakwejk\Portal\Upload\UploadInterface;

interface UploadSaverExceptionInterface
{
    /**
     * @return UploadInterface
     */
    public function getUpload();
}
