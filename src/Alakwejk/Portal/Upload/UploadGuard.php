<?php

/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace Alakwejk\Portal\Upload;

class UploadGuard implements UploadGuardInterface
{
    /**
     * @param UploadInterface $upload
     *
     * @return bool
     */
    public function isUploadValid(UploadInterface $upload)
    {
        return null != $upload->getFile() && $upload->getFile() instanceof \SplFileInfo;
    }
}
