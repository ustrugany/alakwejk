<?php

/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace Alakwejk\Portal\Upload\Document;

interface HasDocumentGuardInterface
{
    /**
     * @param HasDocumentInterface $hasDocument
     *
     * @return bool
     */
    public function isUploadValid(HasDocumentInterface $hasDocument);
}
