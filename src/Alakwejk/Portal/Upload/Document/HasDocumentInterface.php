<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Upload\Document;

use Alakwejk\Portal\Upload\HasUploadInterface;

interface HasDocumentInterface extends HasUploadInterface
{
    /**
     * @return DocumentInterface
     */
    public function getDocument();
}
