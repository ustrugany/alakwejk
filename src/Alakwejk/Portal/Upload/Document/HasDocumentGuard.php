<?php

/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace Alakwejk\Portal\Upload\Document;

use Alakwejk\Portal\Upload\UploadGuard;

class HasDocumentGuard implements HasDocumentGuardInterface
{
    /**
     * @param HasDocumentInterface $hasDocument
     *
     * @return bool
     */
    public function isUploadValid(HasDocumentInterface $hasDocument)
    {
        if (null == $hasDocument->getDocument() || !$hasDocument->getDocument() instanceof DocumentInterface) {
            return false;
        }

        return (new UploadGuard())->isUploadValid($hasDocument->getDocument());
    }
}
