<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Upload\Document;

use Alakwejk\Portal\Upload\UploadInterface;

interface DocumentInterface extends UploadInterface
{
}
