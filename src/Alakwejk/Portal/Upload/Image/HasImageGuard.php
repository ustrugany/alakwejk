<?php

/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace Alakwejk\Portal\Upload\Image;

use Alakwejk\Portal\Upload\UploadGuard;

class HasImageGuard implements HasImageGuardInterface
{
    /**
     * @param HasImageInterface $hasImage
     *
     * @return bool
     */
    public function isUploadValid(HasImageInterface $hasImage)
    {
        if (null == $hasImage->getImage() || !$hasImage->getImage() instanceof ImageInterface) {
            return false;
        }

        return (new UploadGuard())->isUploadValid($hasImage->getImage());
    }
}
