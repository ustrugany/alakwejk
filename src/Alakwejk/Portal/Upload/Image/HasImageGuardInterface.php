<?php

/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace Alakwejk\Portal\Upload\Image;

interface HasImageGuardInterface
{
    /**
     * @param HasImageInterface $hasUpload
     *
     * @return bool
     */
    public function isUploadValid(HasImageInterface $hasUpload);
}
