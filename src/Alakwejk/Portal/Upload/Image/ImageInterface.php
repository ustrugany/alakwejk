<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Upload\Image;

use Alakwejk\Portal\Upload\UploadInterface;

interface ImageInterface extends UploadInterface
{
    /**
     * @return int
     */
    public function getHeight();

    /**
     * @return int
     */
    public function getWidth();
}
