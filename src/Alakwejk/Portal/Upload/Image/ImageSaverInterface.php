<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Upload\Image;

use Alakwejk\Portal\Upload\UploadSaverInterface;

interface ImageSaverInterface extends UploadSaverInterface
{
}
