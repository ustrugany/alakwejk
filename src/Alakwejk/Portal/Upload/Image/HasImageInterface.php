<?php

/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */
namespace Alakwejk\Portal\Upload\Image;

use Alakwejk\Portal\Upload\HasUploadInterface;

interface HasImageInterface extends HasUploadInterface
{
    /**
     * @return ImageInterface
     */
    public function getImage();
}
