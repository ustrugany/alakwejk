<?php
/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */

namespace Alakwejk\Portal\Vote;
use Alakwejk\Portal\Post\PostInterface;


/**
 * Interface VoteRepositoryInterface
 * @package Alakwejk\Portal\Vote
 */
interface VoteRepositoryInterface
{
    /**
     * @param PostInterface $post
     * @return int
     */
    public function findPostRating(PostInterface $post);
}