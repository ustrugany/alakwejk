<?php
/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */

namespace Alakwejk\Portal\Vote;

use Alakwejk\Portal\User\UserInterface;
use Alakwejk\Portal\Post\PostInterface;

interface VoteInterface
{
    /**
     * @return UserInterface
     */
    public function getUser();

    /**
     * @return PostInterface
     */
    public function getPost();

    /**
     * @return int
     */
    public function getValue();
}