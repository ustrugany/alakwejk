tinymce.init({
    height : 300,
    selector:'textarea',
    language : 'pl',
    menubar : true,
    plugins: 'preview paste table code link',
    tools: "inserttable preview",
    toolbar: [
        "undo redo | styleselect | bold italic | link image" | "alignleft aligncenter alignright" | "preview paste"
    ]
});
