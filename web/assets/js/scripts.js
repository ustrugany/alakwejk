jQuery(document).ready(function(){
    var $collectionHolder = $("#event_type_ticketsPools");
    var $addLink = $collectionHolder.parents('.form-group').find('.js-add-item');
    $collectionHolder.data('js-index', $collectionHolder.find('.js-item').length);

    $addLink.on('click', function(e) {
        e.preventDefault();

        var prototype = $collectionHolder.data('prototype');
        var index = $collectionHolder.data('js-index');
        var element = prototype.replace(/__name__/g, index);
        $collectionHolder.data('js-index', index + 1);

        $collectionHolder.append(element);
    });

    $collectionHolder.on('click', '.js-remove-item', function(e) {
        e.preventDefault();
        $(this).parents('.js-item').remove();
    });

    if (($modal = $('body').find('div.modal')).length) {
        $modal.find('.btn.btn-default').on('click', function() {
            $('body').find('[data-target="#myModal"]').parents('form').submit();
        });
    }
});